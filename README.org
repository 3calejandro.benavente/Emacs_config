* Emacs config 

*This is how it look (you can see more images in the folder call images)*  

[[./images/Captura de pantalla de 2017-12-07 15-43-50.png]]

** Basic  Configuracion

*** Melpa repository

De este repositorio sacaremos la mayoria de paquetes - [[https://melpa.org/#/][Melpa]]

- Up-to-date packages built on our servers from upstream source
- Installable in any Emacs with 'package.el' - no local version-control tools needed
- Curated - no obsolete, renamed, forked or randomly hacked packages
- Comprehensive - more packages than any other archive
- Automatic updates - new commits result in new packages
- Extensible - contribute recipes via github, and we'll build the packages
 
#+BEGIN_SRC emacs-lisp 
  (when (>= emacs-major-version 24)
    (require 'package)
    (add-to-list
     'package-archives
     '("melpa" . "http://melpa.org/packages/") t)
    (add-to-list
     'package-archives
     '("melpa-stable" . "http://melpa-stable.milkbox.net/packages/") t)
  )
#+END_SRC
    
*** Use package

[[https://github.com/jwiegley/use-package][use-package]]

The use-package macro allows you to isolate package configuration in your .emacs file in a way
that is both performance-oriented and, well, tidy. I created it because I have over 80 packages
that I use in Emacs, and things were getting difficult to manage. Yet with this utility my total
load time is around 2 seconds, with no loss of functionality!

#+BEGIN_SRC emacs-lisp
  (unless (featurep 'use-package)
    (package-refresh-contents)
    (package-install 'use-package)
  )
#+END_SRC 

*** Some interface configuration 

Quit ring sound
 
#+BEGIN_SRC emacs-lisp
 (setq ring-bell-function 'ignore)                 
#+END_SRC

Quit all graphical element like tool bar scroll bar etc

#+BEGIN_SRC emacs-lisp 
  (tooltip-mode -1)                                  
  (tool-bar-mode -1)
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
#+END_SRC

Clean startup

#+BEGIN_SRC emacs-lisp
  (setq inhibit-startup-message t)
#+END_SRC

Show lines

#+BEGIN_SRC emacs-lisp
  (global-linum-mode t)
#+END_SRC

TEMA

Comprobar que no hay ningun otro tema activado con M-x customize-themes

[[https://github.com/sjrmanning/darkokai][darkokai]]

#+BEGIN_SRC emacs-lisp
 (use-package darkokai-theme
  :ensure t
  :init (load-theme 'darkokai t))
#+END_SRC

*** Navigation

[[https://www.emacswiki.org/emacs/WindMove][Windmove]] ---> Package that help us to swich betwen panel (instead of use shift arrow use C-c arrow)

#+BEGIN_SRC emacs-lisp
(use-package windmove
  :ensure t
  :bind (("C-c <up>" . windmove-up)
        ("C-c <left>" . windmove-left)
        ("C-c <right>" . windmove-right)
        ("C-c <down>" . windmove-down))
  )
#+END_SRC
 
[[https://github.com/abo-abo/swiper][swiper/counsel/ivy]] --->  is a generic completion mechanism for Emacs. While it operates similarly to other completion schemes such as icomplete-mode, Ivy aims to be more efficient, smaller, simpler, and smoother to use yet highly customizable.

#+BEGIN_SRC emacs-lisp
(use-package counsel
  :ensure t
  )

(use-package swiper
  :ensure t
  :config  ;; Configuration for swiper
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq enable-recursive-minibuffers t)
    (global-set-key "\C-s" 'swiper)
    (global-set-key (kbd "C-c C-r") 'ivy-resume)
    (global-set-key (kbd "<f6>") 'ivy-resume)
    (global-set-key (kbd "M-x") 'counsel-M-x)
    (global-set-key (kbd "C-x C-f") 'counsel-find-file)
    (global-set-key (kbd "<f1> f") 'counsel-describe-function)
    (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
    (global-set-key (kbd "<f1> l") 'counsel-find-library)
    (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
    (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
    (global-set-key (kbd "C-c g") 'counsel-git)
    (global-set-key (kbd "C-c j") 'counsel-git-grep)
    (global-set-key (kbd "C-c k") 'counsel-ag)
    (global-set-key (kbd "C-x l") 'counsel-locate)
    (global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
    (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)
    ))

#+END_SRC             

[[https://github.com/justbur/emacs-which-key][which-key]] help us to remember shortcut

#+BEGIN_SRC emacs-lisp
(use-package which-key
  :ensure t
  :init (which-key-mode)
  )
#+END_SRC   

[[https://github.com/jaypei/emacs-neotree][neotree]] ---> emacs plugin to show tree directory

#+BEGIN_SRC emacs-lisp
  (use-package neotree
    :ensure t
    :config
    (global-set-key [f8] 'neotree-toggle)
    (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
  )            
#+END_SRC             

** Programing configuration

*** Basic packages

[[https://github.com/auto-complete/auto-complete][auto-complete]] for emacs

#+BEGIN_SRC emacs-lisp
(use-package auto-complete
  :ensure t
  :config (ac-config-default)
#+END_SRC             

[[https://github.com/joaotavora/yasnippet][yasnippet]] ---> snippet for emacs 

Maybe you will need to install other yasnippet package (I don't remember the name
but just write M-x install-package yasnippet... and is one of theme)

#+BEGIN_SRC emacs-lisp
(use-package yasnippet
  :ensure t
  :init (yas-global-mode 1)
)
#+END_SRC             

*** Python ide 

[[https://github.com/jorgenschaefer/elpy][Elpy]] ---> programing enviroment for emacs

You have to install --> pip install jedi flake8 autopep8 yapf rope

#+BEGIN_SRC emacs-lisp
(use-package elpy
  :ensure t
  :init (elpy-enable)
#+END_SRC             

*** Haskell

[[https://github.com/haskell/haskell-mode][Haskell-mode]] ---> haskell mode for emacs

#+BEGIN_SRC emacs-lisp
(use-package haskell-mode
  :ensure t
  :mode "\\.hs\\'"
  :config 
  (add-hook 'haskell-mode-hook 'turn-on-haskell-doc)
  (add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
)
#+END_SRC   

** Look packages

[[https://github.com/domtronn/all-the-icons.el][all-the-icons]] ---> theme for the icons

remember to use M-x all-the-icons-install-fonts

#+BEGIN_SRC emacs-lisp
(use-package all-the-icons
  :ensure t
)
#+END_SRC             

[[https://julien.danjou.info/projects/emacs-packages][rainbow-mode]] 

#+BEGIN_SRC emacs-lisp
(use-package rainbow-mode
  :ensure t
  :init  
  (add-hook 'prog-mode-hook #'rainbow-mode)
)
#+END_SRC             

[[https://github.com/Fanael/rainbow-delimiters][rainbow-delimiters]]

#+BEGIN_SRC emacs-lisp
(use-package rainbow-delimiters
  :ensure t
  :init
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
)
#+END_SRC             

** Git

[[https://github.com/magit/magit][magit]] ---> git control version GUI

#+BEGIN_SRC emacs-lisp
(use-package magit
  :ensure t
)
#+END_SRC             

** ORG - Mode

[[https://github.com/sabof/org-bullets][org-bullets]] 

#+BEGIN_SRC emacs-lisp
(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
)
#+END_SRC             
