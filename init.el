;; Add melpa package server dev and stable 

(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   '("melpa" . "http://melpa.org/packages/") t)
  (add-to-list
   'package-archives
   '("melpa-stable" . "http://melpa-stable.milkbox.net/packages/") t)
  (package-initialize))

;; use-package load

(unless (featurep 'use-package)
  (package-refresh-contents)
  (package-install 'use-package)
  )

;; GUI configuration 

(setq ring-bell-function 'ignore)                  ;; no anoying sounds
(tooltip-mode -1)                                  ;; don't show gui things 
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(setq inhibit-startup-message t)
(global-linum-mode t)
;; Navigation shift arrow FK OP

(use-package windmove
  :ensure t
  :bind (("C-c <up>" . windmove-up)
        ("C-c <left>" . windmove-left)
        ("C-c <right>" . windmove-right)
        ("C-c <down>" . windmove-down))
  )


;; theme 

;;(use-package darkokai-theme
;;  :ensure t
;;  :init (load-theme 'darkokai t))

(use-package doom-themes
  :ensure t
  :init (load-theme 'doom-one t)
  :config
  (doom-themes-neotree-config)
  (doom-themes-org-config)
)


;; emacs identation

(use-package highlight-indent-guides
  :ensure t    
  :config
  (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
  (setq highlight-indent-guides-method 'character)
  (global-set-key [f7] 'highlight-indent-guides-mode)
  )

;; all-the-icons ---> icon theme

(use-package all-the-icons
  :ensure t
)

;; Neo-tree

(use-package neotree
  :ensure t
  :config
  (global-set-key [f8] 'neotree-toggle)
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
)

;; swiper and counsel (more swiper but you need counsel to make swiper work idk why but like this work)
;; ---> package for autocomplete FK OP

(use-package counsel
  :ensure t
)

(use-package swiper
  :ensure t
  :config  ;; Configuration for swiper
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq enable-recursive-minibuffers t)
    (global-set-key "\C-s" 'swiper)
    (global-set-key (kbd "C-c C-r") 'ivy-resume)
    (global-set-key (kbd "<f6>") 'ivy-resume)
    (global-set-key (kbd "M-x") 'counsel-M-x)
    (global-set-key (kbd "C-x C-f") 'counsel-find-file)
    (global-set-key (kbd "<f1> f") 'counsel-describe-function)
    (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
    (global-set-key (kbd "<f1> l") 'counsel-find-library)
    (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
    (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
    (global-set-key (kbd "C-c g") 'counsel-git)
    (global-set-key (kbd "C-c j") 'counsel-git-grep)
    (global-set-key (kbd "C-c k") 'counsel-ag)
    (global-set-key (kbd "C-x l") 'counsel-locate)
    (global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
    (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)
    ))

;;sudo-edit

(use-package sudo-edit
  :ensure t)
)

;; Which-Key package how show key combination

(use-package which-key
  :ensure t
  :init (which-key-mode)
  )

;; yasnippet package ---> use in different emacs modes like elpy

(use-package yasnippet
  :ensure t
  :init (yas-global-mode 1)
)

;; auto-complete package

(use-package auto-complete
  :ensure t
  :config (ac-config-default)
)

;; elpy ---> python enviromente for emacs -yasnippet- -flycheck- ...

(use-package elpy
  :ensure t
;;  :mode "\\.hs\\'"
  :init (elpy-enable)
)

;; haskell-mode --- haskell enviroment for emacs 

(use-package haskell-mode
  :ensure t
  :mode "\\.hs\\'"
  :config 
  (add-hook 'haskell-mode-hook 'turn-on-haskell-doc)
  (add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
)

(use-package rainbow-mode
  :ensure t
  :init  
  (add-hook 'prog-mode-hook #'rainbow-mode)
)

(use-package rainbow-delimiters
  :ensure t
  :init
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
)

(use-package magit
  :ensure t
)
  

;; ORG PACKAGES --->
;;

(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
)

;;(org-babel-do-load-languages
;; 'org-babel-load-languages
;; '((dot . t)
;;   (latex . t)
;;   (java . t)
;;   (sh . t)
;;   (python . t)
;;))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(elpy-modules
   (quote
    (elpy-module-company elpy-module-eldoc elpy-module-flymake elpy-module-pyvenv elpy-module-yasnippet elpy-module-django elpy-module-sane-defaults)))
 '(package-selected-packages (quote (use-package))))

;; WORK IN PROGRES :(

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

